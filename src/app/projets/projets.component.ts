import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProjectsService} from '../services/projects.service';
import {error} from '@angular/compiler/src/util';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-projets',
  templateUrl: './projets.component.html',
  styleUrls: ['./projets.component.css']
})
export class ProjetsComponent implements OnInit, OnDestroy {

  projects = [];
  projectsSubscription: Subscription;


  constructor(
    private projectsService: ProjectsService
  ) { }

  ngOnInit() {
    this.projectsSubscription = this.projectsService.projectsSubject.subscribe(
      (data: any) => {
        this.projects = data;
      }
    );
    this.projectsService.emitProjects();
  }


  getFinishState(index) {
    if (this.projects[index].finish) {
      return 'green';
    } else {
      return 'red';
    }
  }

  ngOnDestroy() {
    this.projectsSubscription.unsubscribe();
  }

}
