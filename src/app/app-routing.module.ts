import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {PresentationComponent} from './presentation/presentation.component';
import {CvComponent} from './cv/cv.component';
import {ContactComponent} from './contact/contact.component';
import {ProjetsComponent} from './projets/projets.component';
import {AdminDashboardComponent} from './admin/admin-dashboard/admin-dashboard.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'presentation', component: PresentationComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'cv', component: CvComponent },
  { path: 'projets', component: ProjetsComponent },
  { path: 'admin/dashboard', component: AdminDashboardComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
