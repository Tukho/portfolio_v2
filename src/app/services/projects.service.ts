import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  projects = [
    {
      title: 'Jeu Pluie Météorite',
      description: 'Jeu vidéo simple crée en Python, afin de découvrir Pygame. Notions abordées : Collision, Mouvements, etc...',
      category: 'Python',
      picture: 'assets/pictures/bgCometPY.jpg',
      lienGit: 'https://github.com/Ketzatl/Projet_Python_Pygame_JeuCometes',
      lienProjet: '',
      finish: false
    },
    {
      title: 'Réseau Social Inter-Campus',
      description: 'Application mobile de réseau social entre les différents Campus Academy en France.',
      category: 'Angular,HTML5,CSS3, Bootstrap',
      picture: 'assets/pictures/reseauxSociaux.jpg',
      lienGit: '',
      lienProjet: '',
      finish: false
    },
    {
      title: 'Jeu Ballon Flottant',
      description: 'Jeu vidéo très simple crée en Python afin de découvrir Pygame. On y aborde la gestion de "collision".',
      category: 'Python',
      picture: 'assets/pictures/ballonFlottant.jpg',
      lienGit: 'https://github.com/Ketzatl/Projet_Python_Pygame_JeuBallonFlottant',
      lienProjet: '',
      finish: true
    },
    {
      title: 'Application GRTGaz',
      description: 'Application présentant l\'entreprise GRTGaz, ses activités, ses infrastructures et ses projets pour le futur. Apprentissage Angular 9, Firebase, les Web Services et la relation entre les composants',
      category: 'Angular9, Firebase, HTML5, CSS3, Bootstrap, API rest, OPENDatas',
      picture: 'assets/pictures/grtGaz.jpg',
      lienGit: '',
      lienProjet: '',
      finish: false
    },
    {
      title: 'Portfolio Angular9',
      description: 'Une nouvelle version de mon Portfolio, cette fois en Angular 9, framework que je travaille pendant mon stage et que j\'aborderais l\'année prochaine lors de ma 2nde année de Bachelor',
      category: 'Angular9, Firebase, HTML5, CSS3, Bootstrap',
      picture: 'assets/pictures/coffee.jpg',
      lienGit: '',
      lienProjet: 'https://morgan-nott.2strong.fr',
      finish: false
    },
    {
      title: 'Portfolio HTML5,CSS3, JS',
      description: 'Une première version de mon Portfolio. Utilisation de HTML5, CSS3 et Javascript pour l\'animation. Gestion des messages de recommandation avec PHPmyadmin',
      category: 'HTML5, CSS3, Javascript, PHPmyAdmin',
      picture: 'assets/pictures/portfolio.jpg',
      lienGit: 'https://github.com/Ketzatl/Campus_Contest_Fevrier2020',
      lienProjet: 'https://morgan-nott.fr',
      finish: false
    },
    {
      title: 'Projet BarBars',
      description: 'Projet tenant lieu d\'exercice d\'apprentissage Angular 9, Web Services etc...',
      category: 'Angular9, Bootstrap, HTML5, CSS3, Javascript, Firebase',
      picture: 'assets/pictures/portfolio.jpg',
      lienGit: 'https://github.com/Ketzatl/Angular_BarBars',
      lienProjet: '',
      finish: false
    },
    {
      title: 'Projet Agence Immo',
      description: 'Projet tenant lieu d\'exercice d\'apprentissage Angular 9, Web Services, Firebase, et gestion d\'une partie Admin',
      category: 'Angular9, Bootstrap, HTML5, CSS3, Javascript, Firebase',
      picture: 'assets/pictures/villaSF.jpg',
      lienGit: 'https://github.com/Ketzatl/Projet_AgenceImmo',
      lienProjet: '',
      finish: true
    }
  ];

  projectsSubject = new Subject<any[]>();

  constructor() { }

  emitProjects() {
    this.projectsSubject.next(this.projects);
  }
}
