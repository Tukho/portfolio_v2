import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {

  mailPerso = 'morgan.nott@students.campus.academy';
  linkedinPerso = 'linkedin.com/in/morgan-nott/';
  githubPerso = 'github.com/Morgan';

  constructor() { }

  ngOnInit(): void {
  }

}
